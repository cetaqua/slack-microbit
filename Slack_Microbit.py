# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 09:25:33 2019
@author: lpouget
"""

#%% SIMPLE PRINT OF SERIAL DATA
import serial, time

port = "COM3"
baud = 115200
s = serial.Serial(port)
s.baudrate = baud

while True:
    data = s.readline()
    print (data)
    time.sleep(3)
    

#%% FINAL CODE - PRINT SERIAL DATA, GRAPH SERIAL DATA, SAVE TO CSV
#last version of code
#correspond to code "Serial_com2" in micro:bit
#by serial you provide a line, that can be cut to read the variables it contains
#once the python code run, we can upload new code in microbit!
#issue with USB pairing in microbit code editor
# if the microbit code editor is on, we cannot run the python code (serial COM3 not available), we have
# to close the microbit editor and disconect the USB cable 
#issue with reading and sending both humidity and temperature from DHT11
#issue with dealing with the bytes received by serial...
  
#we import the different library     
import serial, time
import pandas as pd
from datetime import datetime
import csv
import ast

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

#now will be the time 
now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
print("now =", now)

#we create a new dataframe with the column corresponding to the signal receive
df = pd.DataFrame(columns = ["hour","temp_dht11","temp_micro","light_micro","test"])

#initiate PLOT
plot_window = 20
y_var = np.array(np.zeros([plot_window]))
plt.ion()
fig, ax = plt.subplots()
line, = ax.plot(y_var,'-o')
plt.title("light_micro")
plt.ylabel("light level")


#load serial data
port = "COM3"
baud = 115200
s = serial.Serial(port)
s.baudrate = baud


while True:
    try:
        #READ serial data every 3 seconds
        data = s.readline() 
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        data = data[0:14]
        data = data.decode()
        data = "["+data+"]"
        data = ast.literal_eval(data)
        data = list(map(int, data))
        print(now,data)
        time.sleep(3)
        #create a dataframe with the data and append it to general df
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        data_ = [[now] + data]
        df2 = pd.DataFrame(data_, columns =["hour","temp_dht11","temp_micro","light_micro","test"] )
        df = df.append(df2,ignore_index=True)
         
        #create a CSV file with the data - careful don´t keep it open with excel (with notepad ok)
        with open("test_data.csv","a", newline='') as f:
            writer = csv.writer(f,delimiter=",")
            writer.writerow([now,data])
        
        #PLOT in real time
        y_var = np.append(y_var,data[2])
        y_var = y_var[1:plot_window+1]
        line.set_ydata(y_var)
        ax.relim()
        ax.autoscale_view()
        fig.canvas.draw()
        fig.canvas.flush_events()
    
        
    except:
        raise
        print("Connection Interruption")
        break
    


    
    
    
